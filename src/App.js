import React, { useEffect, useState } from 'react';
import {
  Player,
  BigPlayButton,
  LoadingSpinner,
  ControlBar,
  ReplayControl,
  ForwardControl,
  PlaybackRateMenuButton,
} from 'video-react';

import VideoService from './services/VideoService';

import './App.css';
import 'plyr-react/dist/plyr.css';
import 'video-react/dist/video-react.css';

function App() {
  const [videos, setVideos] = useState([]);
  const [video, setVideo] = useState('');
  useEffect(() => {
    VideoService.getVideo().then((response) => {
      let {
        data: { body: courseVideos },
      } = response;

      courseVideos = courseVideos.map((courseVideo) => {
        courseVideo.playing = false;
        return courseVideo;
      });

      setVideos(courseVideos);
    });
  }, []);
  console.log(videos);
  return (
    <div className='p-4 w-full sm:w-2/3 mx-auto'>
      <h1 className='text-center font-bold text-2xl mb-8'>nltraining app</h1>
      {/* <VideoPlayer source={video} /> */}
      <Player autoPlay poster='/assets/poster.png' src={video} autoplay>
        <LoadingSpinner />
        <BigPlayButton position='center' />
        <ControlBar autoHide={false}>
          <ReplayControl seconds={5} order={2.1} />
          <ForwardControl seconds={5} order={3.1} />
          <PlaybackRateMenuButton order={7} rates={[1.5, 1, 0.5]} />
        </ControlBar>
      </Player>
      <h3 className='text-xl mt-4'>Flutter Widget of the Week Course</h3>
      <ul className='mt-4'>
        {videos.map((video, index) => {
          return (
            <li
              key={index}
              className={`border rounded mt-1 p-3 cursor-pointer border-gray-200 ${
                video.playing ? 'bg-blue-500 text-white' : ''
              }`}
              onClick={() => {
                const courseVideos = videos.map((video, videoIndex) => {
                  video.playing = false;
                  console.log(videoIndex);
                  console.log(index);
                  if (videoIndex === index) {
                    console.log('playing');
                    video.playing = true;
                  }
                  return video;
                });
                setVideos(courseVideos);
                setVideo(video.url);
              }}
            >
              <p>{video.name}</p>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default App;
