import axios from 'axios';

const axiosClient = axios.create();

const service = {
  getVideo() {
    return axiosClient.get(
      'https://wqrz5xmxxj.execute-api.ap-south-1.amazonaws.com/nltraining-course-api/course-videos',
    );
  },
};

export default service;
